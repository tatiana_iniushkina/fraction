package ru.ita.fraction;

/**
 * Created by ITA on 25.12.2014.
 */
public class Fraction {
    private int up;
    private int down;

    public Fraction(int up, int down) {
        this.up = up;
        this.down = down;
    }

    public int getUp() {
        return up;
    }

    public void setUp(int up) {
        this.up = up;
    }

    public int getDown() {
        return down;
    }

    public void setDown(int down) {
        this.down = down;
    }
    @Override
    public String toString() {
        return "Fraction{" +
                "up=" + up +
                ", down=" + down +
                '}';
    }

}
